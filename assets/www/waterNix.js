/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

moment.lang('en', {
    calendar : {
        lastDay : '[Yesterday]',
        sameDay : '[Today]',
        nextDay : '[Tomorrow]',
        lastWeek : '[last] dddd',
        nextWeek : 'dddd',
        sameElse : 'L'
    }
});

/* Global State */
var _WATER_TEST_MODE = true;
var applicationStorage = {};
var leakWizardActive = false;

var WNTabs = [WNTabDashboard, WNTabReporting, WNTabSaving, WNTabKnowledge];

function doOnOrientationChange()
{
	if(leakWizardActive) {
		showLeakDetectionWizard();
		return false;
	} else {
		callOpenTabHandler($("div.tabs > section.active > p.title > a"));
	}
	console.log("Screen rotated");
}


function callOpenTabHandler(linkElement) {
	var tabName = $(linkElement).attr("data-wnTab");
	if(tabName != undefined && tabName != '') {
		window[tabName].open();
	}
}

/*
 * Setup
 */
function onDeviceReady() {
	applicationStorage = window.localStorage;
	
	$.when(WNDatabase.initialize()).done(function() {
		$.each(WNTabs, function(index, tab) {
			tab.setUp();
		});
		setUpLeakDetectionWizard();
		
		$("div.accordion > section > p.title").click(function (e) {
		    if ($(this).parent().hasClass("active")) {
			    $(this).parent().removeClass("active");
			    return false;
		    };
		}); 
	});
	
	window.addEventListener('orientationchange', doOnOrientationChange);
	document.addEventListener("pause", flushTrackings, false);
	document.addEventListener("resume", function() { StarTrack.track("open")}, false);
	
};

$(function() {
	// Add openTab-Handlers to tab links
	$("section > p.title > a").click(function(e) {
		e.preventDefault();
		callOpenTabHandler(this);
	});
	
	// To make sure all templates are loaded. Not nice, but does the job.
	setTimeout(function() {
	StarTrack.initialize(wnTrackSpots);
	StarTrack.registerEvent("open", "App was opened");
	StarTrack.registerEvent("rmRead", "A Reading was deleted");
	StarTrack.registerEvent("rmReadReq", "A button to delete a reading was pressed");
	StarTrack.registerEvent("savAct", "Water saving method was activated");
	StarTrack.registerEvent("savDeact", "Water saving method was deactivated");
	
	StarTrack.onEachNTrackings(10, flushTrackings);
	StarTrack.track("open"); }, 1500);
});

function refreshMoreDataMessage(element) {
	$(element).text("You need three meter readings in 14 days to see how much water you use.");
	element.show();
}

/*
 *	Utility functions
 */
jQuery.removeFromArray = function(value, arr) {
    return jQuery.grep(arr, function(elem, index) {
        return elem != value;
    });
};

function flushTrackings() {
	writeToFile("wnLog.txt", StarTrack.flushToString());
}

function writeToFile(filename, string) {
	var fail = function(err) {
		console.log(err);
	}
	
	var gotFileWriter = function(writer) {
		writer.onwriteend = function(evt) {
			console.log("Interaction track successfully flushed to file")
		}
		writer.seek(writer.length);
		writer.write(string);
	}
	
	var gotFileEntry = function(fileEntry) {
		fileEntry.createWriter(gotFileWriter, fail);
	}
	
	var gotFS = function(fs) {
		fs.root.getFile(filename, {create: true, exclusive: false}, gotFileEntry, fail);
	};
	
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
}