/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

function convertTimeseriesToFlotFormat(timeSeries) {
	var result = [];
	return $.map(timeSeries, function(element, index) {
		return [ [ element["timestamp"].endOf("day").valueOf(), element["usage"] ] ];
	});
}

var WNDatabase = (function() {
	var TESTMODE = _WATER_TEST_MODE;
	var initialized = false;
	var database = {};

	var databaseName = "waterNix-dev";
	var databaseVersion = 0.1;
	var databaseDisplayName = "WaterNix Dev";
	var databaseSize = 1000000;
	
	var ENOUGH_DATA_THRESHOLD = 3; // items
	var ENOUGH_DATA_TIMESPAN = 14; // days
	
	var rawTestData = [23200, 23940, 24320, 24830, 25600, 26000, 26750];//, 26670, 27120, 27420, 27810, 28230, 28670, 29170];
	var average = 300;
	
	function populateDB(transactionContext) {
		if(TESTMODE) {
			transactionContext.executeSql("DROP TABLE IF EXISTS usage");
			transactionContext
					.executeSql("CREATE TABLE IF NOT EXISTS usage(timeStamp int PRIMARY KEY, usage int)");
			for(i = 0; i < rawTestData.length; i++) {
				var testDataScript = storeReadingScript(moment().subtract('days', i+1).subtract('hours', 1), 
														rawTestData[rawTestData.length - i - 1]);
				transactionContext.executeSql(testDataScript);
			}
			transactionContext
			.executeSql("CREATE TABLE IF NOT EXISTS calls(organization TEXT, name TEXT, number TEXT, reason TEXT, outcome TEXT, timestamp int)");
		} else {
			transactionContext
				.executeSql("CREATE TABLE IF NOT EXISTS usage(timeStamp int PRIMARY KEY, usage int)");
			transactionContext
				.executeSql("CREATE TABLE IF NOT EXISTS calls(organization TEXT, name TEXT, number TEXT, reason TEXT, outcome TEXT, timestamp int)");
		}
	}
	
	function storeReadingScript(timestamp, reading) {
		return "INSERT INTO usage VALUES (" + timestamp.valueOf() + "," + reading + ");";
	}
	
	function executeReadingsQuery(query, postQueryProcessing) {
		var d = new $.Deferred();
		var processing = function(tx, results) {
			var result = [];
			var len = results.rows.length;
			for ( var i = 0; i < len; i++) {
				result.push({
					"timestamp" : moment(results.rows.item(i).timeStamp),
					"usage" : results.rows.item(i).usage
				});
			}
			if (postQueryProcessing) {
				result = postQueryProcessing(result);
			}
			d.resolve(result);
		}; 
		
		executeQuery(query, processing);
		return d;
	}

	function executeAggregateReadingsQuery(query, postQueryProcessing) {
		var d = new $.Deferred();
		var processing = function(tx, results) {
			var result = [];
			var len = results.rows.length;
			for ( var i = 0; i < len; i++) {
				result.push({
					"week" : results.rows.item(i).week,
					"year" : results.rows.item(i).year,
					"readings" : results.rows.item(i).readings
				});
			}
			if (postQueryProcessing) {
				result = postQueryProcessing(result);
			}
			d.resolve(result);
		}; 
		
		executeQuery(query, processing);
		return d;
	}
	
	function executeCallsQuery(query, postQueryProcessing) {
		var d = new $.Deferred();
		var processing = function(tx, results) {
			var result = [];
			var len = results.rows.length;
			for ( var i = 0; i < len; i++) {
				var item = results.rows.item(i);
				var object = {};
				var attributes = ["timestamp", "name", "organization", "number", "reason", "outcome"];
				for(var key in item) {
					if($.inArray(key, attributes)) {
						object[key] = item[key] == "NULL" ? "" : item[key];
					}
				}
				object.timestamp = moment(object.timestamp);
				result.push(object);
			}
			if (postQueryProcessing) {
				result = postQueryProcessing(result);
			}
			d.resolve(result);
		}; 
		
		executeQuery(query, processing);
		return d;
	}
	
	
	function executeQuery(query, processResult) {
		database.transaction(function(tx) {
			tx.executeSql(query, [], processResult, errorHandler);
		}, errorHandler);
	}
	
	function executeStoreQuery(query) {
		var d = new $.Deferred();
		
		var processResult = function(tx, results) {
			d.resolve(true);
		}
		
		var outerErrorHandler = function(err) {
			errorHandler(err);
			d.resolve(false);
		}
		
		database.transaction(function(tx) {
			tx.executeSql(query, [], processResult, outerErrorHandler);
		}, outerErrorHandler);
		
		return d;
	}
	
	function getReadingFromTimeToTime(startTime, endTime, postQueryProcessing) {
		var query = 'SELECT * FROM usage WHERE timestamp >= '
						+ startTime.valueOf().toString() 
						+ " AND timestamp <= "
						+ endTime.valueOf().toString();
		return executeReadingsQuery(query, postQueryProcessing);
	}
	
	function getRightMostEarlierReading(timestamp, postQueryProcessing) {
		var query = 'SELECT * from usage WHERE timestamp <= ' + timestamp.valueOf().toString() + ' order by timestamp desc limit 1';
		return executeReadingsQuery(query, postQueryProcessing);
	}
	
	function getLeftMostLaterReading(timestamp, postQueryProcessing) {
		var query = 'SELECT * from usage WHERE timestamp >= ' + timestamp.valueOf().toString() + ' order by timestamp asc limit 1';
		return executeReadingsQuery(query, postQueryProcessing);
	}
	
	function getReadingsInternal(noOfReadings, postQueryProcessing) {
		var query = "SELECT * from usage ORDER BY timestamp DESC limit " + noOfReadings.toString();
		return executeReadingsQuery(query, postQueryProcessing);
	}
	
	function getCallsInternal(postQueryProcessing) {
		var query = "SELECT * from calls;"
		return executeCallsQuery(query, postQueryProcessing);
	}
	
	function getWeeklyReadingsCount(postQueryProcessing) {
		var query = "Select week, year, count(usage) as readings from (SELECT strftime('%W' , datetime(timestamp / 1000, 'unixepoch')) AS week, strftime('%Y' , datetime(timestamp / 1000, 'unixepoch')) AS year, usage from usage) group by week, year ORDER BY year ASC, week ASC;";
		return executeAggregateReadingsQuery(query, postQueryProcessing);
	}
	
	
	function storeMeterReadingInternal(timestamp, meterReading) {
		var query = storeReadingScript(timestamp, meterReading);
		return executeStoreQuery(query);
	}
	
	function deleteReadingInternal(timestamp) {
		var query = "DELETE from usage where timestamp = " + timestamp.toString();
		return executeQuery(query);
	}
	
	function storeCallInternal(organization, name, number, reason, outcome) {
		var organization = organization == "" ? "NULL" : organization;
		var name = name == "" ? "NULL" : name;
		var number = number == "" ? "NULL" : number;
		var reason = reason == "" ? "NULL" : reason;
		var outcome = outcome == "" ? "NULL" : outcome;
		var timestamp = moment().valueOf().toString();
		var query = "INSERT INTO calls VALUES ('" + organization + "', '" + name + "', '" + number + "', '" + reason + "', '" + outcome + "'," + timestamp + ");";
		return executeStoreQuery(query);
	}
	
	function errorHandler(err) {
		console.log("Error processing SQL: " + err.message);
	}
	
	function databasePopulationSuccessHandler() {
		initialized = true;
	}
	
	function endOfDay(day) {
		return moment(day).endOf("day");
	}
	
	function startOfDay(day) {
		 return moment(day).startOf("day");
	}
	
	function getSingleResult(results) {
		return results[0];
	}
	
	return {
		initialize : function() {
			var d = new $.Deferred();
			database = window.openDatabase(databaseName, databaseVersion,
					databaseDisplayName, databaseSize);
			
			database.transaction(populateDB, errorHandler, function() {
				databasePopulationSuccessHandler();
				d.resolve();
			});
			return d;
		},
		storeMeterReading : function(time, reading) {
			return storeMeterReadingInternal(time, reading);
		},
		deleteReading : function(timestamp) {
			StarTrack.track("rmRead", timestamp);
			return deleteReadingInternal(timestamp);
		},
		getUsageForDay: function(day) {
			var startDay = startOfDay(day);
			var endDay = endOfDay(day);
			var d = new $.Deferred();
			$.when(getRightMostEarlierReading(startDay, getSingleResult),
				   getReadingFromTimeToTime(startDay, endDay, getSingleResult),
				   getLeftMostLaterReading(endDay, getSingleResult))
			.done(function(readingDayBefore, readingDay, readingDayAfter) {
				var result = 0;
				
				if(readingDay) {
					var hourlyUsageFirstHalf = (readingDay.usage - readingDayBefore.usage) / moment.duration(readingDay.timestamp - readingDayBefore.timestamp).asHours();
					result = result + moment.duration(readingDay.timestamp - startDay).asHours() * hourlyUsageFirstHalf;
					
					var hourlyUsageSecondHalf = (readingDayAfter.usage - readingDay.usage) / moment.duration(readingDayAfter.timestamp - readingDay.timestamp).asHours();
					result = result + moment.duration(endDay - readingDay.timestamp).asHours() * hourlyUsageFirstHalf;
				} else {
					var hourlyAverageUsage = (readingDayAfter.usage - readingDayBefore.usage) / moment.duration(readingDayAfter.timestamp - readingDayBefore.timestamp).asHours();
					result = 24 * hourlyAverageUsage;
				}
				
				//TODO: absolute no readings available
				
				d.resolve({"timestamp" : day, "usage" : result});
			});
			return d;
		},
		getWeeklyReadingsCount : function() {
			return getWeeklyReadingsCount();
		},
		getReadingOfLatestReading : function() {
			return getRightMostEarlierReading(moment(), function(results) {
				if(results.length == 0) {
					return false;
				} else {
					return results[0].usage;
				}
			})
		},
		getTimeOfLatestReading : function(day) {
			return getRightMostEarlierReading(moment(), function(results) {
				if(results.length == 0) {
					return false;
				} else {
					return results[0].timestamp;
				}
			})
		},
		getTimeOfOldestReading : function() {
			return getLeftMostLaterReading(moment(0), function(results) {
				return results[0].timestamp;
			})
		},
		getDailyUsageFromTo : function(startDate, endDate) {
			var d = new $.Deferred();
			var that = this;
			
			$.when(this.getTimeOfOldestReading())
			.done(function(oldestReading) {
				var numberOfDays = endDate.diff(startDate, "days");
				var queries = [];
				 for(var i = 0; i <= numberOfDays; i++) {
					 var date = moment(startDate).add("days", i).startOf("day");
					 if(date.valueOf() > oldestReading.valueOf()) {
						 // If there are not enough readings we just omit them...
						 queries.push(that.getUsageForDay(date));
					 }
				 }
				 $.when.apply(null, queries).done(function(results) {
					 d.resolve(arguments);
				 });
			});
				
			return d;
		},
		getDailyUsageToNowForDays : function(numberOfDays) {
			var d = new $.Deferred();
			var that = this;
			$.when(this.getTimeOfLatestReading(moment()), this.getTimeOfOldestReading())
			 .done(function(lastReading, oldestReading) {
				 
				 var queries = [];
				 for(var i = 1; i <= numberOfDays; i++) {
					 var date = moment(lastReading).subtract("days", i).startOf("day");
					 if(date.valueOf() > oldestReading.valueOf()) {
						 // If there are not enough readings we just omit them...
						 queries.push(that.getUsageForDay(date));
					 }
				 }
				 $.when.apply(null, queries.reverse()).done(function(results) {
					 d.resolve(arguments);
				 });
			 });
			
			return d;
		},
		getOverallAverageSeriesForDays : function(numberOfDays, overlap) {
			var d = new $.Deferred();
			
			// Cheap hack to get right amount of day tuples
			$.when(this.getAverageOverDays(numberOfDays), this.getTimeOfLatestReading(moment()))
			.done(function(average, latestReading) {
				var result = [];
				var timestamp = moment(latestReading).subtract("days", 1).startOf("day");
				timestamp.add("days", overlap - 1);
				for (var i = 0; i < numberOfDays + overlap; i++) {
					result.push( { "timestamp" : moment(timestamp), "usage" : average });
					timestamp.subtract("days", 1);
				}
				
				d.resolve(result);
			})
			
			return d;
		},
		getAverageOverDays : function(numberOfDays) {
			var d = new $.Deferred();
			$.when(this.getDailyUsageToNowForDays(numberOfDays))
			.done(function(x) {
				var results = [].slice.call(arguments)[0]; //To copy arguments in a real array
				var result = 0;
				$.each(results, function(index, element) {
					result += element.usage;
				})
				result = result / results.length;
				d.resolve(result);
			});
			return d;
		},
		getReadings : function(noOfReadings) {
			return getReadingsInternal(noOfReadings);
		},
		enoughData : function() {
			var d = new $.Deferred();
			$.when(getReadingFromTimeToTime(moment().subtract("days", ENOUGH_DATA_TIMESPAN), moment()))
			.done(function(readings) {
				d.resolve(readings.length >= ENOUGH_DATA_THRESHOLD);
			});
			return d;
		},
		/* Report Calls API */
		storeCall : function(organization, name, number, reason, outcome) {
			return storeCallInternal(organization, name, number, reason, outcome);
		},
		getCalls : function() {
			return getCallsInternal();
		}
	}
})();