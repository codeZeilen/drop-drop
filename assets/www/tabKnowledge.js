/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

var WNTabKnowledge = function() {
	
	return {
		"setUp" :  function() {
			Handlebars.registerHelper('topic_text', function() {
				  return new Handlebars.SafeString(
				    this.text
				  );
			});
			
			var source   = $("#knowledge-chapters-template").html();
			var template = Handlebars.compile(source);
			var html = template(WNKnowledgeChapters);
			
			$("#tab-knowledge").append(html);
		},
		"refresh" : function() {
			
		},
		"open" : function() {
			$("#tab-knowledge").foundation('section', 'reflow');
		}	
	};
}();