/*
	Drop Drop Project
	iCOMMS Group - University of Cape Town
	Initial Author: Patrick Rein (pre), patrick.rein@gmail.com
 */

var WNTabSaving = function () {

	var populateWaterSavingMethodsList = function() {
		console.log("Populating Saving Methods List");
		
		var source   = $("#water-saving-methods-template").html();
		var template = Handlebars.compile(source);
		var html = template({ "methods" : WNWaterSavingMethods});
		
		$('div#water-saving-methods').append(html);
		
		console.log("Populating Saving Methods List Done");
	}

	var triggerWaterSavingMethod = function(e) {
		var methodIndex = parseInt($(e).attr("data-method-index"),10);
		if(e.checked) {
			StarTrack.track("savAct", methodIndex);
			WNWaterSavingModel.addSavingMethod(WNWaterSavingMethods[methodIndex]);
		} else {
			StarTrack.track("savDeact", methodIndex);
			WNWaterSavingModel.removeSavingMethod(WNWaterSavingMethods[methodIndex]);
		}
		refreshModelChart();
		refreshMonetarianSaving();
		return false;
	}

	var refreshMonetarianSaving = function() {
		$.when(WNWaterSavingModel.calculateProjectedSavingsForAMonth()).done(function(projectedSavings) {
			$("span#model-money-saving-placeholder").text(projectedSavings);
		});
	}

	var refreshModelChart = function() {
		console.log("Starting refreshing Model Chart");
		var projectionLength = 4;
		var pastLength = 6;
		$.when(	WNDatabase.getDailyUsageToNowForDays(pastLength),
				WNWaterSavingModel.calculateProjectedDailySeries(projectionLength),
				WNWaterSavingModel.calculateNormalProjectedDailySeries(projectionLength))
		.done(function(resultUsageSeries, projectedSeries, normalProjectedSeries) {
					
			var usageSeries = convertTimeseriesToFlotFormat(resultUsageSeries);
			var projectedSeries = convertTimeseriesToFlotFormat(projectedSeries);
			var normalProjectedSeries = convertTimeseriesToFlotFormat(normalProjectedSeries);
			
			$.when(WNDatabase.getOverallAverageSeriesForDays(resultUsageSeries.length, projectionLength + 1))
			.done(function(averageSeries) {
				console.log("Database operations finished, starting configuring Chart");
				
				var averageSeries = convertTimeseriesToFlotFormat(averageSeries);
				
				var series1 = {
					data : usageSeries,
					label : "Your Usage",
					color : "#2489ce",
					bars : {
						show : true,
						barWidth : 18*60*60*1000
					}
				};
				var average = {
					data : averageSeries,
					color : "#b8ffc0",
					lines : {
						show : true,
						lineWidth : 0
					}
				};
				var spark = {
					data : [ usageSeries[usageSeries.length - 1] ],
					color: '#ff0000',
					bars : {
						show : true,
						barWidth : 18*60*60*1000
					},
					color : '#ff0000'
				};
				var projected = {
					data  : projectedSeries,
					color : '#FCC74C',
					label : 'Projected Use',
					bars : {
						show : true,
						barWidth : 18*60*60*1000
					}
				};
				var normalProjected = {
					data  : normalProjectedSeries,
					color : '#AAD5F2',
					label : 'Normal Use',
					bars : {
						show : true,
						barWidth : 18*60*60*1000
					}
				}
				
				$.plot($("#model-chart-placeholder"), [ average, series1, normalProjected, projected, spark], {
					xaxis : {
						mode : "time",
						tickSize : [3, "day"]
					},
					grid : {
						borderWidth : {
							"top" : 0,
							"left" : 1,
							"bottom" : 1,
							"right" : 0
						}
					},
					bars : {
						align : "center"
					},
					legend : {
						container : "#model-legend-placeholder",
						sorted : "descending",
						noColumns : 3
					}
				});
				console.log("Done Drawing Chart!");
			});
		});
	};

	
	var _refresh = function() {
		$.when(WNDatabase.enoughData())
		.done(function(enoughData) {
			if(enoughData) {
				$("#water-saving-message-box").hide();
				$("#water-saving-chart").show();
				refreshModelChart();
				refreshMonetarianSaving();
				console.log("Done Refreshing Chart!");
			} else {
				$("#water-saving-chart").hide();
				refreshMoreDataMessage($("#water-saving-message-box"));
			}
		});
	}
	
	var self = this;
	return {
		"setUp" : function () {
			Handlebars.registerHelper('method_actuall_savings', function() {
				//TODO this.saving should be called with realistic average
				  return new Handlebars.SafeString(
				    this.saving() == 0 ? "" : "(Saves around " + Math.round(this.saving(250)) + "l per Day)"
				  );
			});
			
			Handlebars.registerHelper('method_id', function() {
				  return new Handlebars.SafeString(
						  " data-method-index=\"" + this.id +"\" "
				  );
			});
			
			Handlebars.registerHelper('checkbox_name', function() {
				return new Handlebars.SafeString(
						  "methodBox" + this.id
				); 
			})
			
			populateWaterSavingMethodsList();
			_refreshSavingTab();
			
			$('.accordion > section > p.title > input').click(function(e){
				   e.stopPropagation();
				   triggerWaterSavingMethod(e.target);
			});
		},
		"refresh" : function () {
			_refresh();
		},
		"open" : function () {
			_refresh();
		}
		
	};
}();

function setUpWaterSavingTab() {
	WNTabSaving.setUp();
}

function onOpenWaterSavingTab() {
	WNTabSaving.open();
}

function _refreshSavingTab() {
	WNTabSaving.refresh();
}
